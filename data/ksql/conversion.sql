CREATE OR REPLACE STREAM reddit_json (
    id VARCHAR,
    title VARCHAR,
    author VARCHAR,
    subreddit VARCHAR,
    created_utc INT,
    permalink VARCHAR,
    url VARCHAR,
    thumbnail VARCHAR,
    over_18 BOOLEAN,
    hidden BOOLEAN,
    score INT,
    num_comments INT
) WITH (KAFKA_TOPIC='reddit-posts', VALUE_FORMAT='json');

CREATE OR REPLACE STREAM reddit_as_avro
WITH (VALUE_FORMAT='avro')
AS SELECT * FROM reddit_json EMIT CHANGES;
