select sum(case when "OVER_18" = true then 1 end)                             as "adult content",
       count(*)                                                                  "total",
       (sum(case when "OVER_18" = true then 1 end) / cast(count(*) as float)) as "ratio"
from "REDDIT_AS_AVRO";

select distinct "CREATED_UTC" / 1000,
                count(*) over (
                    partition by "CREATED_UTC" / 10 -- div by 10 to get a 10-second average
                    rows between unbounded preceding and unbounded following
                    )
from "REDDIT_AS_AVRO";

select round(avg("count"), 2) from
(select distinct count(*) over (partition by "CREATED_UTC" / 60 rows between unbounded preceding and unbounded following) from "REDDIT_AS_AVRO") as tmp;

select "OVER_18",
       cast(count(distinct "AUTHOR") as float) / count("ID")
from "REDDIT_AS_AVRO"
group by "OVER_18";

select max("CREATED_UTC") - min("CREATED_UTC")
from "REDDIT_AS_AVRO";
